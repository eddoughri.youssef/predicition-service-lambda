import pickle
import json

def normlization(features, nrml_z, inp):
    data = [];
    index = 0;
    for i in range(len(features)):
        if(features[i]=="Status"):
            data.append(inp[i]);
        else:
            mean, std = nrml_z[index];
            data.append((inp[i] - mean) / std);
            index+=1;
    return [data];

def checkValid(inp, features):
    for i in features:
        if(i == "Status"):
            if(inp[i] != "Yes" and inp[i] != "No"):
                return False;
            continue;
        if(i == "Population" and (inp[i] < 1000000 or inp[i] > 100000000)):
            return False;
        if(type(inp[i]) != float and type(inp[i]) != int):
            return False;
    return True;

def extract(inp, features):
    newArray = [];
    for i in features:
        if( i == "Status"):
            newArray.append( 1 if inp[i] == "Yes" else -1);
            continue;
        newArray.append(float(inp[i]));
    return newArray;


def lambda_handler(request, context):
    if(not ("data" in request)):
        return {
            'statusCode': 400,
            'body': "Invalid Request: request didn't contain input data"
        }
    user_input = request["data"];
    # Load the model
    with open('modelStored.pkl', 'rb') as f:
        features, nrml_z, model = pickle.load(f);
    # Predict
    if(len(user_input) != len(features)):
        return {
            'statusCode': 400,
            'body': "Invalid input: wrong number of features"
        }
    elif(not checkValid(user_input, features)):
        return {
            'statusCode': 400,
            'body': "Invalid Input: used a wrong type of feature"
        }
    # transform input to an array
    row_input = extract(user_input, features);
    # normalize input
    data = normlization(features, nrml_z, row_input);
    z = model.predict(data);
    return {
        'statusCode': 200,
        'body': json.dumps(z[0][0])
    }